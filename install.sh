#!/usr/bin/env bash

# Inspired by DasGeek
# See: https://github.com/dasgeekchannel/AutoInstallBashScripts
# by danielw2904
# Tested w/ 18.04 LTS 
# Version: 2019.01.09 (1.1)

## Define some variables
install='sudo apt install -y'
installn='sudo apt-get install -y --no-install-recommends'
snap='sudo snap install'
upgrade='sudo apt upgrade -y'
update='sudo apt update'
ppa='sudo add-apt-repository --yes'
dl_dir=/home/$USER/Downloads/apps

chk_dl_dir(){
if [ ! -d "$dl_dir" ]; then
	echo "Creating directory:"
	$dl_dir 
	mkdir "$dl_dir"
fi
}

get_snap(){
	if command -v snap 2>/dev/null; then
		echo "Snap is installed. Proceeding..."
	else
		echo "Snap is not installed but required. "
	dialog --title "Install Snap?" \
	--yesno "Would you like to install Snap?" 7 60
	response=$?
	case $response in
   		0) echo "Installing Snap."
			$install snapd
			;;
   		1) echo "Not installing Snap."
		   ;;
   		255) echo "Exiting."
		   exit 1
		   ;;
	esac
fi
}

get_conda(){
	chk_dl_dir
	if command -v conda 2>/dev/null; then
		echo "Conda is already installed."
	else
		cd "$dl_dir" || exit 1
		if [ ! -f "$dl_dir/Miniconda3-latest-Linux-x86_64.sh" ]; then
			echo "Miniconda installer not found! Downloading..."
			wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
		fi
		chmod +x "$dl_dir/Miniconda3-latest-Linux-x86_64.sh"
		"$dl_dir/Miniconda3-latest-Linux-x86_64.sh"
		cd || exit 1
		exec bash
	fi
}

get_rstudio(){
	chk_dl_dir
	cd "$dl_dir" || exit 1
	# modified from: https://github.com/rocker-org/rocker-versioned/blob/master/rstudio/3.5.0/Dockerfile
	RSTUDIO_LATEST=$(wget --no-check-certificate -qO- https://s3.amazonaws.com/rstudio-desktop/current.ver)
	if [ ! -f "$dl_dir/rstudio-xenial-${RSTUDIO_LATEST}-amd64.deb" ]; then
		echo "RStudio installer not found! Downloading..."
		wget "https://download1.rstudio.org/rstudio-xenial-${RSTUDIO_LATEST}-amd64.deb"
	fi
	echo "Installing RStudio v. $RSTUDIO_LATEST"
	$install "$dl_dir/rstudio-xenial-${RSTUDIO_LATEST}-amd64.deb"
}


chk_conda(){
if command -v conda 2>/dev/null; then
	echo "Conda is installed. Proceeding..."
else
	echo "Conda is not installed but required. "
	dialog --title "Install Miniconda?" \
	--yesno "Would you like to download and install Miniconda3? (requires script restart)" 7 60
	response=$?
	case $response in
   		0) echo "Installing Miniconda."
			get_conda
			;;
   		1) echo "Not installing Miniconda."
		   ;;
   		255) echo "Exiting."
		   exit 1
		   ;;
	esac
fi
}

#Update and Upgrade
echo "Updating and Upgrading"
$update
$upgrade

$install dialog 
cmd=(dialog --title "Setup Installer" --separate-output --checklist "Please Select Software You Want To Install:" 22 80 16)
options=(
		1_R "	Install R-base" off
		2_R "	Install R-recommended" off
		3_R "	Install marutter 3.4 PPAs" off 
		4_R "	Install R-base 3.5 & c2d4u (marutter)" off
		5_R "	Install Rcpp & Armadillo" off
		6_R "	Install Recommended R dependencies" off
		7_R "	Install Geospatial R dependencies" off
		8_R "	Install Openblas" off
		9_R "	Install Intel MKL (from Eddelbuettel; Requires restart)" off
		10_R "	Install Econometrics w/ R packages (~200MB)" off
		1_writing "	Install texlive-full" off
		2_writing "	Install texlive-base" off
		1_coding "	Install emacs25" off
		2_coding "	Install vs-code" off
		3_coding " 	Install emacs-goodies" off
		4_coding "	Install RStudio 1.463" off
		5_coding "	Install git" off
		1_drivers "	Install Nvidia-410" off
		1_misc "	Install flatpak" off
		2_misc "	Install fish shell v.3" off
		3_misc "	Set Capslock to additional Esc" off
		1_python " 	Install Miniconda3 (requires script restart)" off
		2_python " 	Install PyTorch 1.0 w/ CUDA (requires conda)" off
		3_python "      Python emacs setup" off
		1_end "		Clean up download directory" off
	)

choices=$("${cmd[@]}" "${options[@]}" 2>&1 >/dev/tty)		
clear 

for choice in $choices
do
	case $choice in
		1_R)
			echo "Installing R-base"
			$install r-base
			;;
		2_R)
			echo "Installing R-recommended"
			$install r-recommended
			;;
		3_R)
			echo "Adding marutter 3.4 PPAs"
			$ppa ppa:marutter/rrutter
			$ppa ppa:marutter/c2d4u
			$upgrade
			;;
		4_R)
			echo "Adding marutter and installing R 3.5"
			$ppa ppa:marutter/rrutter3.5
			$ppa ppa:marutter/c2d4u3.5
			$update
			if command -v R 2>/dev/null; then
				$upgrade
			else
				$install r-base
			fi	
			;;
		5_R)
			echo "Installing Rcpp & Armadillo"
			$install r-cran-rcpp
			$install r-cran-rcpparmadillo
			;;
		6_R)
			echo "Installing Recommended dependencies"
			$installn libxml2-dev \
				libcairo2-dev \
				libsqlite-dev \
				libpq-dev \
				libssh2-1-dev\
				libxml2-dev\
        		libcurl4-openssl-dev\
        		libssl-dev\
        		gcc-7\
        		g++-7\
        		gfortran-7\
				pandoc \
    			pandoc-citeproc \
    			libcairo2-dev \
    			libxt-dev \
    			xtail
			;;
		7_R)
			echo "Installing Geospatial dependencies"
			# from: https://github.com/fineprint-global/docker-rstudio/blob/master/2018.11.05/Dockerfile
			$installn libgdal-dev \
				libgeos-dev \
				r-cran-openssl \
				r-base-dev \
				r-cran-rjava \
				liblzma-dev \
				openjdk-8-jre \
				openjdk-8-jdk \
				r-cran-spatial \
				r-cran-spatstat \
				r-cran-spdep
			;;
		8_R)
			echo "Installing Openblas"
			$install libopenblas-base
			;;
		9_R)
			echo "Installing Intel MKL"
			if [ ! -f "$dl_dir/edd-script.sh" ]; then
				chk_dl_dir
				cd "$dl_dir" || exit 1
				wget -O edd-script.sh https://raw.githubusercontent.com/eddelbuettel/mkl4deb/master/script.sh
				chmod +x edd-script.sh
			fi
			sudo ./edd-script.sh
			echo "Please restart your computer for changes to take effect."
			;;
		10_R)
			$install r-cran-aer \
				r-cran-arm \
				r-cran-bayesm \
				r-cran-bbmle \
				r-cran-bms \
				r-cran-bit64 \
				r-cran-brglm \
				r-cran-broom \
				r-cran-car \
				r-cran-caret \
				r-cran-coda \
				r-cran-data.table \
				r-cran-dplyr \
				r-cran-drr \
				r-cran-dynlm \
				r-cran-e1071 \
				r-cran-forcats \
				r-cran-gam \
				r-cran-ggplot2 \
				r-cran-glmnet \
				r-cran-glue \
				r-cran-hmisc \
				r-cran-knitr \
				r-cran-lavaan \
				r-cran-lme4 \
				r-cran-lmertest \
				r-cran-lubridate \
				r-cran-matchit \
				r-cran-mvtnorm \
				r-cran-nlme \
				r-cran-openssl \
				r-cran-psych \
				r-cran-purrr \
				r-cran-quantreg \
				r-cran-rms \
				r-cran-rocr \
				r-cran-rquantlib \
				r-cran-shiny \
				r-cran-spam \
				r-cran-sparsem \
				r-cran-stringr \
				r-cran-survey \
				r-cran-tidyr \
				r-cran-truncdist \
				r-cran-tseries \
				r-cran-urca \
				r-cran-vgam \
				r-cran-wdi \
				r-cran-xtable \
				r-cran-xts \
				r-cran-zelig
			;;
		1_writing)
			$install texlive-full
			;;
		2_writing)
			$install texlive-base
			;;
		1_coding)
			$install emacs25
			;;
		2_coding)
			get_snap
			if command -v snap 2>/dev/null; then
				$snap vscode --classic
			else
				$install code
			fi
			;;
		3_coding)
			$install emacs-goodies-el \
				elpa-projectile \
				auctex \
				ess 
			;;
		4_coding)
			get_rstudio
			;;
		5_coding)
			$install git
			;;
		1_drivers)
			$ppa ppa:graphics-drivers/ppa
			$update
			$install nvidia-driver-410
			;;
		1_misc)
			$install flatpak
			;;
		2_misc)
			$ppa ppa:fish-shell/release-3
			$update
			$install fish
			dialog --title "Oh-my-fish" \
			--yesno "Would you like to download and install OMF?" 7 60
			response=$?
			case $response in
   				0) echo "Installing OMF."
					$install curl
					chk_dl_dir
					cd "$dl_dir" || exit 1
					curl -L https://get.oh-my.fish > install
					sha_file=$(sha256sum install)
					sha_given=$(curl https://raw.githubusercontent.com/oh-my-fish/oh-my-fish/master/bin/install.sha256)
					if [ "$sha_file" == "$sha_given" ]; then
					echo "Checksums match. Proceeding..."
						fish install
						omf install bobthefish
					else
						echo "Checksums did not match. Please make sure the download is legitimate."
						exit 1
					fi
					;;
   				1) echo "Not installing OMF."
		   			;;
   				255) echo "Exiting."
		   			exit 1
		   			;;
			esac
			chsh -s /usr/bin/fish "$USER"
			;;
		3_misc)
			dconf write /org/gnome/desktop/input-sources/xkb-options "['caps:escape']"
			;;
		1_python)
			get_conda
			;;
		2_python)
			chk_conda
			if command -v conda 2>/dev/null; then
				envname=$(dialog --title "PyTorch Setup" \
				--inputbox "Enter name for PyTorch Environment " 0 0 2>&1 1>/dev/tty);
				echo "Creating  $envname"
				conda create --name "$envname"
				echo "Installing pytorch with CUDA 10"
				source activate "$envname"
				conda install pytorch torchvision cuda100 -c pytorch
				source deactivate
			else
				echo "Cannot install PyTorch without Miniconda!"
			fi
			;;
		3_python)
		    $install python3-pip
		    pip3 install jedi
# flake8 for code checks
		    pip3 install flake8
# and autopep8 for automatic PEP8 formatting
		    pip3 install autopep8
# and yapf for code formatting
		    pip3 install yapf

		1_end)
			if [ "$(ls -A $dl_dir)" ]; then
				echo "Removing contents of  $dl_dir"
				rm "$dl_dir"/*
			else
    			echo "Download directory is empty."
			fi
			
	esac
done
