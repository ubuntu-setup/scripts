#! /bin/bash

install='sudo apt install -y'

$install samba
$install python3-smbc
$install smbclient

echo "Download Canon driver manually!"
echo "https://www.canon-europe.com/support/products/imagerunner/imagerunner-advance-c5250.aspx?type=drivers&language=EN&os=Linux%20(64-bit)"

echo "Add the following below [global] in smb.conf."

echo "client min protocol = SMB2"

echo "client max protocol = SMB3"

echo "Add this now? [y/n]"
read edit
if [[ $edit == [Yy]* ]];then
sudo nano /etc/samba/smb.conf
fi

echo "Restart now? [y/n]"
read restart
if [[ $restart == [Yy]* ]];then
reboot
fi